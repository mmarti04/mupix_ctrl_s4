-- Last Change: M.Mueller, November 2020 (muellem@uni-mainz.de)
-- there are TWO instances of this entity: one in mp_block, one in mp_datapath
-- TODO: check if things are compiled away correctly in 2nd instance_name .. if not --> new file mupix_reg_mapping_datapath.vhd

-- At some point we might want to generate this file automatically from mupix_registers.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mupix_reg_mapping is
port (
    i_clk156                    : in  std_logic;
    i_reset_n                   : in  std_logic;

    i_reg_add                   : in  std_logic_vector(7 downto 0);
    i_reg_re                    : in  std_logic;
    o_reg_rdata                 : out std_logic_vector(31 downto 0);
    i_reg_we                    : in  std_logic;
    i_reg_wdata                 : in  std_logic_vector(31 downto 0);

    -- inputs  156--------------------------------------------
    -- ALL INPUTS DEFAULT TO (n*4-1 downto 0 => x"CCC..", others => '1')
    i_lvds_data_valid           : in  std_logic_vector(35 downto 0) := x"CCCCCCCCC"; -- lvds alignment to mupix chips ok

    -- outputs 156--------------------------------------------
    o_mp_lvds_link_mask         : out std_logic_vector(35 downto 0); -- lvds link mask
    o_mp_datagen_control        : out std_logic_vector(31 downto 0); -- control register for the mupix data gen
    o_mp_readout_mode           : out std_logic_vector(31 downto 0); -- Invert ts, degray, chip ID numbering, tot mode, ..

    o_mp_ctrl_data              : out std_logic_vector(32*5 + 31 downto 0);
    o_mp_fifo_write             : out std_logic_vector( 5 downto 0);
    o_mp_fifo_clear             : out std_logic_vector( 5 downto 0);
    o_mp_ctrl_enable            : out std_logic_vector( 5 downto 0);
    o_mp_ctrl_chip_config_mask  : out std_logic_vector(11 downto 0);
    o_mp_ctrl_invert_29         : out std_logic;
    o_mp_ctrl_invert_csn        : out std_logic;
    o_mp_ctrl_slow_down         : out std_logic_vector(31 downto 0)--;
);
end entity;

architecture rtl of mupix_reg_mapping is
    constant MP_CTRL_ENABLE_REGISTER_W      :  integer := 16#40#;
        constant WR_BIAS_BIT                :  integer := 0;
        constant WR_CONF_BIT                :  integer := 1;
        constant WR_VDAC_BIT                :  integer := 2;
        constant WR_COL_BIT                 :  integer := 3;
        constant WR_TEST_BIT                :  integer := 4;
        constant WR_TDAC_BIT                :  integer := 5;
        constant CLEAR_BIAS_FIFO_BIT        :  integer := 6;
        constant CLEAR_CONF_FIFO_BIT        :  integer := 7;
        constant CLEAR_VDAC_FIFO_BIT        :  integer := 8;
        constant CLEAR_COL_FIFO_BIT         :  integer := 9;
        constant CLEAR_TEST_FIFO_BIT        :  integer := 10;
        constant CLEAR_TDAC_FIFO_BIT        :  integer := 11;

    constant MP_CTRL_BIAS_REGISTER_W        :  integer := 16#41#;
    constant MP_CTRL_CONF_REGISTER_W        :  integer := 16#42#;
    constant MP_CTRL_VDAC_REGISTER_W        :  integer := 16#43#;
    constant MP_CTRL_COL_REGISTER_W         :  integer := 16#44#;
    constant MP_CTRL_TEST_REGISTER_W        :  integer := 16#45#;
    constant MP_CTRL_TDAC_REGISTER_W        :  integer := 16#46#;

    constant MP_CTRL_SLOW_DOWN_REGISTER_W   :  integer := 16#47#;
    constant MP_CTRL_CHIP_MASK_REGISTER_W   :  integer := 16#48#;
    constant MP_CTRL_INVERT_REGISTER_W      :  integer := 16#49#;
        constant MP_CTRL_INVERT_29_BIT      :  integer := 0;
        constant MP_CTRL_INVERT_CSN_BIT     :  integer := 1;

            constant MP_READOUT_MODE_REGISTER_W     :  integer := 16#60#;
        constant INVERT_TS_BIT              :  integer := 0; -- if set: TS is inverted
        constant INVERT_TS2_BIT             :  integer := 1; -- if set: TS2 is inverted
        constant GRAY_TS_BIT                :  integer := 2; -- if set: TS is grey-decoded
        constant GRAY_TS2_BIT               :  integer := 3; -- if set: TS2 is grey-decoded
        -- bits to select different chip id numbering modes (layers etc.)
        -- not in use at the moment
        subtype  CHIP_ID_MODE_RANGE         is integer range 5 downto 4;
        -- bits to select different TOT calculation modes
        -- Default is to send TS2 as TOT
        subtype  TOT_MODE_RANGE             is integer range 8 downto 6;
    constant MP_LVDS_LINK_MASK_REGISTER_W   :  integer := 16#61#;
    constant MP_LVDS_LINK_MASK2_REGISTER_W  :  integer := 16#62#;
    constant MP_LVDS_DATA_VALID_REGISTER_R  :  integer := 16#63#;
    constant MP_LVDS_DATA_VALID2_REGISTER_R :  integer := 16#64#;
    constant MP_DATA_GEN_CONTROL_REGISTER_W :  integer := 16#65#;
        -- hit output probability is 1/(2^(MP_DATA_GEN_HIT_P_RANGE+1)) for each cycle where a hit could be send
        -- (in 125 MHz minus protocol overhead sorter -> merger)
        subtype  MP_DATA_GEN_HIT_P_RANGE    is integer range 3 downto 0;
        constant MP_DATA_GEN_FULL_STEAM_BIT :  integer := 4; -- if set: hit output probability is 1
        constant MP_DATA_GEN_SYNC_BIT       :  integer := 5; -- if set: generator seed is the same on all boards else: generator seed depends on ref_addr from backplane
        constant MP_DATA_GEN_ENGAGE_BIT     :  integer := 16; -- if set: use hits from generator, datapath is not connected to link
        constant MP_DATA_GEN_SORT_IN_BIT    :  integer := 17; -- if set: generated hits are inserted after the data_unpacker
        constant MP_DATA_GEN_ENABLE_BIT     :  integer := 31; -- if set: enable hit generation (set MP_DATA_GEN_ENGAGE_BIT to actually replace sorter output with these hits)


    signal mp_datagen_control       : std_logic_vector(31 downto 0);
    signal mp_readout_mode          : std_logic_vector(31 downto 0);
    signal mp_lvds_link_mask        : std_logic_vector(35 downto 0);
    signal mp_lvds_data_valid       : std_logic_vector(35 downto 0);
    signal mp_ctrl_slow_down        : std_logic_vector(31 downto 0);
    signal mp_ctrl_chip_config_mask : std_logic_vector(31 downto 0);
    signal mp_ctrl_invert_29        : std_logic;
    signal mp_ctrl_invert_csn       : std_logic;
begin

    process (i_clk156, i_reset_n)
        variable regaddr : integer;
    begin
        if (i_reset_n = '0') then 
            mp_datagen_control        <= (others => '0');
            o_mp_ctrl_enable          <= (others => '0');
            mp_ctrl_invert_csn        <= '0';
            
        elsif(rising_edge(i_clk156)) then

            --regs for long paths
            o_mp_lvds_link_mask         <= mp_lvds_link_mask;
            o_mp_datagen_control        <= mp_datagen_control;
            o_mp_readout_mode           <= mp_readout_mode;
            mp_lvds_data_valid          <= i_lvds_data_valid;
            o_mp_ctrl_slow_down         <= mp_ctrl_slow_down;
            o_mp_ctrl_chip_config_mask  <= mp_ctrl_chip_config_mask(11 downto 0);
            o_mp_ctrl_invert_29         <= mp_ctrl_invert_29;
            o_mp_ctrl_invert_csn        <= mp_ctrl_invert_csn;

            regaddr             := to_integer(unsigned(i_reg_add(7 downto 0)));
            o_reg_rdata         <= x"CCCCCCCC";
            o_mp_fifo_write     <= (others => '0');

            -----------------------------------------------------------------
            ---- mupix ctrl -------------------------------------------------
            -----------------------------------------------------------------

            if ( regaddr = MP_CTRL_ENABLE_REGISTER_W and i_reg_we = '1' ) then
                o_mp_fifo_clear  <= i_reg_wdata(CLEAR_TDAC_FIFO_BIT downto CLEAR_BIAS_FIFO_BIT);
                o_mp_ctrl_enable <= i_reg_wdata(WR_TDAC_BIT downto WR_BIAS_BIT);
            end if;

            if ( regaddr = MP_CTRL_CONF_REGISTER_W and i_reg_we = '1' ) then
                o_mp_ctrl_data(WR_CONF_BIT*32 + 31 downto WR_CONF_BIT*32) <= i_reg_wdata;
                o_mp_fifo_write(WR_CONF_BIT) <= '1';
            end if;
            if ( regaddr = MP_CTRL_VDAC_REGISTER_W and i_reg_we = '1' ) then
                o_mp_ctrl_data(WR_VDAC_BIT*32 + 31 downto WR_VDAC_BIT*32) <= i_reg_wdata;
                o_mp_fifo_write(WR_VDAC_BIT) <= '1';
            end if;
            if ( regaddr = MP_CTRL_BIAS_REGISTER_W and i_reg_we = '1' ) then
                o_mp_ctrl_data(WR_BIAS_BIT*32 + 31 downto WR_BIAS_BIT*32) <= i_reg_wdata;
                o_mp_fifo_write(WR_BIAS_BIT) <= '1';
            end if;
            if ( regaddr = MP_CTRL_TDAC_REGISTER_W and i_reg_we = '1' ) then
                o_mp_ctrl_data(WR_TDAC_BIT*32 + 31 downto WR_TDAC_BIT*32) <= i_reg_wdata;
                o_mp_fifo_write(WR_TDAC_BIT) <= '1';
            end if;
            if ( regaddr = MP_CTRL_TEST_REGISTER_W and i_reg_we = '1' ) then
                o_mp_ctrl_data(WR_test_BIT*32 + 31 downto WR_test_BIT*32) <= i_reg_wdata;
                o_mp_fifo_write(WR_test_BIT) <= '1';
            end if;
            if ( regaddr = MP_CTRL_COL_REGISTER_W and i_reg_we = '1' ) then
                o_mp_ctrl_data(WR_COL_BIT*32 + 31 downto WR_COL_BIT*32) <= i_reg_wdata;
                o_mp_fifo_write(WR_COL_BIT)  <= '1';
            end if;

            if ( regaddr = MP_CTRL_SLOW_DOWN_REGISTER_W and i_reg_we = '1' ) then
                mp_ctrl_slow_down <= i_reg_wdata;
            end if;
            if ( regaddr = MP_CTRL_SLOW_DOWN_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata <= mp_ctrl_slow_down;
            end if;

            if ( regaddr = MP_CTRL_CHIP_MASK_REGISTER_W and i_reg_we = '1' ) then
                mp_ctrl_chip_config_mask <= i_reg_wdata;
            end if;
            if ( regaddr = MP_CTRL_CHIP_MASK_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata <= mp_ctrl_chip_config_mask;
            end if;

            if ( regaddr = MP_CTRL_INVERT_REGISTER_W and i_reg_we = '1' ) then
                mp_ctrl_invert_29   <= i_reg_wdata(MP_CTRL_INVERT_29_BIT);
                mp_ctrl_invert_csn  <= i_reg_wdata(MP_CTRL_INVERT_CSN_BIT);
            end if;
            if ( regaddr = MP_CTRL_INVERT_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata(MP_CTRL_INVERT_29_BIT)  <= mp_ctrl_invert_29;
                o_reg_rdata(MP_CTRL_INVERT_CSN_BIT) <= mp_ctrl_invert_csn;
            end if;

            -----------------------------------------------------------------
            ---- datapath ---------------------------------------------------
            -----------------------------------------------------------------

            if ( regaddr = MP_READOUT_MODE_REGISTER_W and i_reg_we = '1' ) then
                mp_readout_mode <= i_reg_wdata;
            end if;
            if ( regaddr = MP_READOUT_MODE_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata <= mp_readout_mode;
            end if;

            if ( regaddr = MP_LVDS_LINK_MASK_REGISTER_W and i_reg_we = '1' ) then
                mp_lvds_link_mask(31 downto 0) <= i_reg_wdata;
            end if;
            if ( regaddr = MP_LVDS_LINK_MASK_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata <= mp_lvds_link_mask(31 downto 0);
            end if;
            if ( regaddr = MP_LVDS_LINK_MASK2_REGISTER_W and i_reg_we = '1' ) then
                mp_lvds_link_mask(35 downto 32) <= i_reg_wdata(3 downto 0);
            end if;
            if ( regaddr = MP_LVDS_LINK_MASK2_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata(3 downto 0) <= mp_lvds_link_mask(35 downto 32);
                o_reg_rdata(31 downto 4)<= (others => '0');
            end if;

            if ( regaddr = MP_LVDS_DATA_VALID_REGISTER_R and i_reg_re = '1' ) then
                o_reg_rdata <= mp_lvds_data_valid(31 downto 0);
            end if;
            if ( regaddr = MP_LVDS_DATA_VALID2_REGISTER_R and i_reg_re = '1' ) then
                o_reg_rdata(3 downto 0) <= mp_lvds_data_valid(35 downto 32);
                o_reg_rdata(31 downto 4)<= (others => '0');
            end if;

            if ( regaddr = MP_DATA_GEN_CONTROL_REGISTER_W and i_reg_we = '1' ) then
                mp_datagen_control <= i_reg_wdata;
            end if;
            if ( regaddr = MP_DATA_GEN_CONTROL_REGISTER_W and i_reg_re = '1' ) then
                o_reg_rdata <= mp_datagen_control;
            end if;
            
        end if;
    end process;
end architecture;
