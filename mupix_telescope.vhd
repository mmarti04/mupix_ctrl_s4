---------------------------------------------------------
-- mupix_telescope: Top Level entity for the Mupix     --
-- telescope RO FPGA                                   --
--                                                     --
-- Derived from the ALTERA golten_top Verilog file     --
--                                                     --
-- Niklaus Berger, Heidelberg University 2013          --
-- nberger@physi.uni-heidelberg.de                     --
--                                                     --
-- Adaptions for MuPix8:                               --
-- Sebastian Dittmeier, Heidelberg University 2017     --
-- dittmeier@physi.uni-heidelberg.de                   --
---------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity mupix_telescope is 
	port (
		--//GPLL-CLK-----------------------------//8 pins
		clkin_50: 			in 	std_logic;    --//2.5V    //50 MHz, also to EPM2210F256
		clkintop_100_p: 	in 	std_logic;    --//LVDS    //100 MHz prog osc
		clkinbot_100_p: 	in 	std_logic;    --//LVDS    //100 MHz prog osc
		clk_125_p: 			in 	std_logic_vector(1 downto 1);    --//LVDS    //125 MHz GPLL-req's OCT
--		clkout_sma:  		out 	std_logic;    --//2.5V    //PLL clock output or GPIO

		--//Character-LCD------------------------//11 pins //--------------------------
		lcd_csn: 				out 	std_logic;             			--//2.5V    //LCD Chip Select
		lcd_d_cn: 				out 	std_logic;            			--//2.5V    //LCD Data / Command Select
		lcd_data: 				inout std_logic_vector(7 downto 0);   --//2.5V    //LCD Data
		lcd_wen: 				out 	std_logic;             			--//2.5V    //LCD Write Enable

		--//User-IO------------------------------//27 pins //--------------------------
		user_dipsw: 			in 	std_logic_vector(7 downto 0);   --//2.5V    //User DIP Switches (TR=0)
		user_led: 				out 	std_logic_vector(15 downto 0);  --//2.5V    //User LEDs
		user_pb: 				in 	std_logic_vector(2 downto 0);   --//2.5V    //User Pushbuttons (TR=0)
		cpu_resetn: 			in 	std_logic;          			--//2.5V    //CPU Reset Pushbutton (TR=0)


		--//HSMC-Port-A--------------------------//107pins //--------------------------
-- Bank 1		
--		A_clkext_fast_front:		out	std_logic;	-- has to be connected to be used!
--		A_clkext_fast_back:		out	std_logic;	-- has to be connected to be used!	
		A_spare_clk_out:			out	std_logic;	-- use as FPGA_RESET_OUT		
		A_spare_clk_in:			in		std_logic;	
		
		hsma_prsntn: 				in 	std_logic;         				--//2.5V    //HSMC Presence Detect Input		
--		hsma_rx_led: 				out 	std_logic;         				--//2.5V    //User LED - Labeled RX
--		hsma_scl: 					out 	std_logic;            			--//2.5V    //SMBus Clock
		hsma_sda: 					inout std_logic;            			--//2.5V    //SMBus Data
--		hsma_tx_led: 				out 	std_logic;         				--//2.5V    //User LED - Labeled TX		

-- Front port	
-- outputs	
		A_spi_ld_front:			out	std_logic;			
		A_spi_clk_front:			out	std_logic;
		A_testpulse_1_front:		out	std_logic;
		A_spi_din_front:			out	std_logic;	
		A_spi_ld_tmp_dac_front:	out	std_logic;	
		A_spi_ld_adc_front:		out	std_logic;	
		A_ctrl_ld_front:			out	std_logic;	
		A_ctrl_clk2_front:		out	std_logic;
		A_ctrl_clk1_front:		out	std_logic;
		A_ctrl_din_front:			out	std_logic;
		A_ctrl_rb_front:			out	std_logic;
		A_ldcol_front:				out	std_logic;		
		A_rdcol_front:				out	std_logic;	
		A_pd_data_front:			out	std_logic;	
		A_ldpix_front:				out	std_logic;
		A_trig_front:				out	std_logic;
		A_syncres_front:			out	std_logic;
		A_clkref_front:			out	std_logic;
-- inputs	
		A_ctrl_dout_front:		in		std_logic;
		A_hit_front:				in		std_logic;		
		A_spi_dout_adc_front:	in		std_logic;
		A_spi_dout_dac_front:	in		std_logic;
		A_dac4_dout_front:		in		std_logic;	
		A_hb_front:					in		std_logic;	
		A_dataout_front:			in		std_logic_vector(3 downto 0);	
-- only front bank			
		A_trig_ttl:					in		std_logic_vector(3 downto 0);	
		A_fpga_rst_out:			in		std_logic;	-- can only be used as input!
		A_fpga_rst_in:				in		std_logic;		
		
-- Back port
-- outputs
		A_spi_ld_back:				out	std_logic;			
		A_spi_clk_back:			out	std_logic;
		A_testpulse_1_back:		out	std_logic;
		A_spi_din_back:			out	std_logic;	
		A_spi_ld_tmp_dac_back:	out	std_logic;	
		A_spi_ld_adc_back:		out	std_logic;	
		A_ctrl_ld_back:			out	std_logic;	
		A_ctrl_clk2_back:			out	std_logic;
		A_ctrl_clk1_back:			out	std_logic;
		A_ctrl_din_back:			out	std_logic;
		A_ctrl_rb_back:			out	std_logic;
		A_ldcol_back:				out	std_logic;		
		A_rdcol_back:				out	std_logic;	
		A_pd_data_back:			out	std_logic;	
		A_ldpix_back:				out	std_logic;
		A_trig_back:				out	std_logic;
		A_syncres_back:			out	std_logic;
		A_clkref_back:				out	std_logic;
-- inputs
		A_ctrl_dout_back:			in		std_logic;
		A_hit_back:					in		std_logic;		
		A_spi_dout_adc_back:		in		std_logic;
		A_spi_dout_dac_back:		in		std_logic;
		A_dac4_dout_back:			in		std_logic;	
		A_hb_back:					in		std_logic;	
		A_dataout_back:			in		std_logic_vector(3 downto 0);			
-- only back bank		
		A_tlu_clk_local:			in		std_logic;	-- for synchronous (AIDA) mode with new TLU!
		A_tlu_busy_local:			out	std_logic;		
		A_tlu_rst_local:			in		std_logic;	
		A_tlu_trg_local:			in		std_logic;		
		A_clkin:						in		std_logic;		



		--//HSMC-Port-B--------------------------//107pins //--------------------------
-- Bank 1		
--		B_clkext_fast_front:		out	std_logic;	-- has to be connected to be used!
--		B_clkext_fast_back:		out	std_logic;	-- has to be connected to be used!
		B_spare_clk_out:			out	std_logic;		
		B_spare_clk_in:			in		std_logic;	
		
		hsmb_prsntn: 				in 	std_logic;         				--//2.5V    //HSMC Presence Detect Input		
--		hsmb_rx_led: 				out 	std_logic;         				--//2.5V    //User LED - Labeled RX
--		hsmb_scl: 					out 	std_logic;            			--//2.5V    //SMBus Clock
		hsmb_sda: 					inout std_logic;            			--//2.5V    //SMBus Data
--		hsmb_tx_led: 				out 	std_logic;         				--//2.5V    //User LED - Labeled TX		

-- Front port	
-- outputs	
		B_spi_ld_front:			out	std_logic;			
		B_spi_clk_front:			out	std_logic;
		B_testpulse_1_front:		out	std_logic;
		B_spi_din_front:			out	std_logic;	
		B_spi_ld_tmp_dac_front:	out	std_logic;	
		B_spi_ld_adc_front:		out	std_logic;	
		B_ctrl_ld_front:			out	std_logic;	
		B_ctrl_clk2_front:		out	std_logic;
		B_ctrl_clk1_front:		out	std_logic;
		B_ctrl_din_front:			out	std_logic;
		B_ctrl_rb_front:			out	std_logic;
		B_ldcol_front:				out	std_logic;		
		B_rdcol_front:				out	std_logic;	
		B_pd_data_front:			out	std_logic;	
		B_ldpix_front:				out	std_logic;
		B_trig_front:				out	std_logic;
		B_syncres_front:			out	std_logic;
		B_clkref_front:			out	std_logic;
-- inputs	
		B_ctrl_dout_front:		in		std_logic;
		B_hit_front:				in		std_logic;		
		B_spi_dout_adc_front:	in		std_logic;
		B_spi_dout_dac_front:	in		std_logic;
		B_dac4_dout_front:		in		std_logic;	
		B_hb_front:					in		std_logic;	
		B_dataout_front:			in		std_logic_vector(3 downto 0);	
-- only front bank			
		B_trig_ttl:					in		std_logic_vector(3 downto 0);	
		B_fpga_rst_out:			in	std_logic;	-- can only be used as input!
		B_fpga_rst_in:				in	std_logic;		
		
-- Back port
-- outputs
		B_spi_ld_back:				out	std_logic;			
		B_spi_clk_back:			out	std_logic;
		B_testpulse_1_back:		out	std_logic;
		B_spi_din_back:			out	std_logic;	
		B_spi_ld_tmp_dac_back:	out	std_logic;	
		B_spi_ld_adc_back:		out	std_logic;	
		B_ctrl_ld_back:			out	std_logic;	
		B_ctrl_clk2_back:			out	std_logic;
		B_ctrl_clk1_back:			out	std_logic;
		B_ctrl_din_back:			out	std_logic;
		B_ctrl_rb_back:			out	std_logic;
		B_ldcol_back:				out	std_logic;		
		B_rdcol_back:				out	std_logic;	
		B_pd_data_back:			out	std_logic;	
		B_ldpix_back:				out	std_logic;
		B_trig_back:				out	std_logic;
		B_syncres_back:			out	std_logic;
		B_clkref_back:				out	std_logic;
-- inputs
		B_ctrl_dout_back:			in		std_logic;
		B_hit_back:					in		std_logic;		
		B_spi_dout_adc_back:		in		std_logic;
		B_spi_dout_dac_back:		in		std_logic;
		B_dac4_dout_back:			in		std_logic;	
		B_hb_back:					in		std_logic;	
		B_dataout_back:			in		std_logic_vector(3 downto 0);			
-- only back bank		
		B_tlu_clk_local:			out	std_logic;
		B_tlu_busy_local:			out	std_logic;		
		B_tlu_rst_local:			in		std_logic;	
		B_tlu_trg_local:			in		std_logic;		
		B_clkin:						in		std_logic		

	);
end mupix_telescope; 



architecture RTL of mupix_telescope is

signal user_pb_db: std_logic_vector(2 downto 0);


signal spi_test_dout : std_logic;
signal spi_test_clock : std_logic;
signal spi_test_cs : std_logic;

	component debouncer
	port(
		clk:			in std_logic;
		din:			in std_logic;
		dout:			out std_logic
		);		
	end  component;

begin

    e_mupix_ctrl : work.mupix_ctrl_dummy
    port map(
        i_clk                       => clkin_50,
        i_start                     => not user_pb_db(1),

        o_spi_clock                 => spi_test_clock,
        o_spi_mosi                  => spi_test_dout,
        o_spi_csn                   => spi_test_cs--,
    );

B_syncres_back <= '0';--spi_test_dout;
B_syncres_front <= '0';-- spi_test_dout;
A_syncres_back <= '0';-- spi_test_dout;
A_syncres_front <= '0';-- spi_test_dout;
A_ctrl_din_front <= spi_test_dout;
A_ctrl_din_back <= spi_test_dout;
B_ctrl_din_front <= spi_test_dout;
B_ctrl_din_back <= spi_test_dout;

--A_ctrl_clk1_front <= spi_test_clock;
--A_ctrl_clk1_back <= spi_test_clock;
--B_ctrl_clk1_front <= spi_test_clock;
--B_ctrl_clk1_back <= spi_test_clock;

A_ctrl_clk2_front <= spi_test_clock;
A_ctrl_clk2_back <= spi_test_clock;
B_ctrl_clk2_front <= spi_test_clock;
B_ctrl_clk2_back <= spi_test_clock;

A_ctrl_ld_front <= spi_test_cs;
A_ctrl_ld_back <= spi_test_cs;
B_ctrl_ld_front <= spi_test_cs;
B_ctrl_ld_back  <= spi_test_cs;

-------------------------------------------------- Clocking -----------------------------------------------------------------

-- Clocks to chips
--A_clkref_front	<= clkin_50;	--clock125out;
--A_clkref_back	<= clkin_50;	--clock125out;
--B_clkref_front	<= clkin_50;	--clock125out;
--B_clkref_back	<= clkin_50;	--clock125out;


user_led(3) <= user_pb_db(0);
user_led(4) <= user_pb_db(1);
user_led(5) <= user_pb_db(2);

-- Debouncers
-- running with on-board oscillator
db1: debouncer
	port map(
		clk		=> clkin_50,
		din		=> user_pb(0),
		dout		=> user_pb_db(0)
		);		

db2: debouncer
	port map(
		clk		=> clkin_50,
		din		=> user_pb(1),
		dout		=> user_pb_db(1)
		);		
		
db3: debouncer
	port map(
		clk		=> clkin_50,
		din		=> user_pb(2),
		dout		=> user_pb_db(2)
		);		

end RTL;
